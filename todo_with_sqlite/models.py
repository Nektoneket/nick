from django.db import models
from django.forms import ModelForm
from django import forms
import json

class ToDodb(models.Model):
	number_of_task = models.CharField(max_length=500)
	name_of_task = models.CharField(max_length=100)
	deadline_of_task = models.CharField(max_length=10)
	importance = models.IntegerField()
	description = models.CharField(max_length=500)
	def __str__(self): 	
		return ' '.join([self.number_of_task, self.name_of_task])

class ToDodbForm(ModelForm):
	class Meta:
		model = ToDodb
		fields = ['number_of_task', 'name_of_task', 'deadline_of_task', 'importance', 'description']

class Get_task_Form(forms.Form):
	number_of_task = forms.CharField(label='number of task', max_length=100)
