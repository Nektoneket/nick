from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^add_to_db/$', views.add_to_db, name='add_to_db'),
	url(r'^see_all_tasks/$', views.see_all_tasks, name='see_all_tasks'),	
	url(r'^see_one_task/$', views.see_one_task, name='see_one_task'),
	url(r'^del_all_tasks/$', views.del_all_tasks, name='del_all_tasks'),
	url(r'^del_one_task/$', views.del_one_task, name='del_one_task'),

	
]
