from django.http import HttpResponse
from django.template import RequestContext, loader
from django.shortcuts import render
from django.http import HttpResponseRedirect
import json
import re

from .models import ToDodb, ToDodbForm, Get_task_Form



def index(request):
	return render(request, 'todo_with_sqlite/index.html')
	

def add_to_db(request):
	
	
	if request.method == 'POST':
		
		form = ToDodbForm(request.POST)
	
		if form.is_valid():			
			new_task = form.save()  
			       						
			return HttpResponseRedirect('/todo_with_sqlite')


	else:
		form = ToDodbForm()

	return render(request, 'todo_with_sqlite/data_entry.html', {'form': form})

def see_all_tasks(request):
	a = ToDodb.objects.all()
	context =  {'list_of': a}

	return render(request, 'todo_with_sqlite/all_tasks.html', context)
	

def see_one_task(request):
	all_tasks = ToDodb.objects.all()
	if request.method == 'POST':
		
		form = Get_task_Form(request.POST)
		
		if form.is_valid():
			data = form.cleaned_data['number_of_task']	
			a = ToDodb.objects.get(number_of_task = data)		
					       						
			return HttpResponse("<html><head></head><body><ul><li>'%s'</li></ul></body></html>"% a)


	else:
		form = Get_task_Form()

	context =  {'list_of': all_tasks, 'form': form}

	return render(request, 'todo_with_sqlite/see_one_task.html', context)

def del_all_tasks(request):
	ToDodb.objects.all().delete()

	return HttpResponse (ToDodb.objects.all())

def del_one_task(request):
	all_tasks = ToDodb.objects.all()
	
	if request.method == 'POST':
		
		form = Get_task_Form(request.POST)
		
	
		if form.is_valid():
			a = form.cleaned_data['number_of_task']	
			ToDodb.objects.filter(number_of_task=a).delete()		
					       						
			return HttpResponseRedirect('/todo_with_sqlite')


	else:
		form = Get_task_Form()

	context =  {'list_of': all_tasks, 'form': form}

	return render(request, 'todo_with_sqlite/del_one_task.html', context)















